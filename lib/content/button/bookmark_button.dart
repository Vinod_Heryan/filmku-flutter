import 'package:flutter/material.dart';

class BookmarkButton extends StatefulWidget {
  const BookmarkButton({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _BookmarkButtonState createState() => _BookmarkButtonState();
}

class _BookmarkButtonState extends State<BookmarkButton> {
  bool isBookmark = false;
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        isBookmark ? Icons.bookmark : Icons.bookmark_border,
        color: Colors.yellowAccent,
      ),
      onPressed: () {
        setState(() {
          if (isBookmark == false) {
            isBookmark = true;
          } else {
            isBookmark = false;
          }
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(isBookmark ? 'Bookmark Saved' : 'Bookmark Deleted'),
            duration: const Duration(seconds: 1),
          ),
        );
      },
    );
  }
}