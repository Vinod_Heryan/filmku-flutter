import 'package:flutter/material.dart';
import 'package:filmku/model/film.dart';
import 'package:filmku/detail_film_screen.dart';

class FilmList extends StatelessWidget {
  const FilmList({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataFilmList.length,
      itemBuilder: (context, index) {
        final TourismPlace film = dataFilmList[index];
        return InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return DetailFilmScreen(
                film: film,
              );
            }));
          },
          child: Card(
            elevation: 6,
            shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(12.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(
                        10.0),
                    child: Image.asset(film.imageAsset),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          film.name,
                          style: const TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'YoungSerif'
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(film.genre),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                          const Text('Score : '),
                          Text(film.score),
                        ]),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
