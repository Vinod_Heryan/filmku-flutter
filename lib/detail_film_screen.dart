import 'package:flutter/material.dart';
import 'package:filmku/model/film.dart';
import 'package:filmku/content/button/favorite_button.dart';
import 'package:filmku/content/button/bookmark_button.dart';

class DetailFilmScreen extends StatelessWidget {
  final TourismPlace film;

  const DetailFilmScreen({Key? key, required this.film}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return DetailFilmScreenMobile(film: film);
      },
    );
  }
}

class DetailFilmScreenMobile extends StatelessWidget {
  final TourismPlace film;

  const DetailFilmScreenMobile({Key? key, required this.film})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Expanded(
                  child: SizedBox(
                    child: ClipRect(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          film.imageAsset,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                const Positioned(
                  top: 25,
                  left: 10,
                  child: FavoriteButton(),
                ),
                const Positioned(
                  top: 25,
                  right: 10,
                  child: BookmarkButton(),
                ),
                Container(
                  width: width,
                  margin: EdgeInsets.only(top: height * 0.5),
                  padding: const EdgeInsets.all(30),
                  decoration: const BoxDecoration(
                    color: Color(0xFFF7F7F7),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Text(
                        film.name,
                        style: const TextStyle(
                          fontSize: 24.0,
                          fontFamily: 'YoungSerif',
                          decoration: TextDecoration.underline,
                          decorationColor: Colors.black,
                          decorationThickness: 2.0,
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      const Divider(
                        height: 1,
                        color: Colors.black,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              const Text('Score : ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text(film.score,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          Text(film.genre,
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold)),
                        ],
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          const Text('Deskripsi : ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(film.description, textAlign: TextAlign.justify),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
