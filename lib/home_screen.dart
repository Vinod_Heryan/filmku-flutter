import 'package:flutter/material.dart';
import 'package:filmku/content/list_film.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Daftar Film',
          style: TextStyle(
            fontFamily: 'YoungSerif',
          ),
        ),
      ),
      body:const FilmList()
    );
  }
}
