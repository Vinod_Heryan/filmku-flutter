class TourismPlace {
  String name;
  String genre;
  String description;
  String score;
  String imageAsset;

  TourismPlace({
    required this.name,
    required this.genre,
    required this.description,
    required this.score,
    required this.imageAsset,
  });
}

var dataFilmList = [
  TourismPlace(
    name: 'Loki',
    genre: 'Drama, Sci-Fi & Fantasy',
    description:
        'After stealing the Tesseract during the events of "Avengers: Endgame," an alternate version of Loki is brought to the mysterious Time Variance Authority, a bureaucratic organization that exists outside of time and space and monitors the timeline. They give Loki a choice: face being erased from existence due to being a "time variant" or help fix the timeline and stop a greater threat.',
    score: '82',
    imageAsset: 'images/loki.jpg',
  ),
  TourismPlace(
    name: 'The Equalizer 3',
    genre: 'Action, Thriller, Crime',
    description:
        'Robert McCall finds himself at home in Southern Italy but he discovers his friends are under the control of local crime bosses. As events turn deadly, McCall knows what he has to do: become his friends protector by taking on the mafia.',
    score: '73',
    imageAsset: 'images/theequalizer.jpg',
  ),
  TourismPlace(
    name: 'Totally Killer',
    genre: 'Horror, Comedy',
    description: 'When the infamous "Sweet Sixteen Killer" returns 35 years after his first murder spree to claim another victim, 17-year-old Jamie accidentally travels back in time to 1987, determined to stop the killer before he can start.',
    score: '72',
    imageAsset: 'images/totally.jpg',
  ),
  TourismPlace(
    name: 'Ballerina',
    genre: 'Action, Crime, Thriller',
    description: 'Grieving the loss of a best friend she couldn`t protect, an ex-bodyguard sets out to fulfill her dear friend`s last wish: sweet revenge.',
    score: '73',
    imageAsset: 'images/ballerina.jpg',
  ),
  TourismPlace(
    name: 'Fair Play',
    genre: 'Thriller, Drama',
    description: 'An unexpected promotion at a cutthroat hedge fund pushes a young couple`s relationship to the brink, threatening to unravel not only their recent engagement but their lives.',
    score: '66',
    imageAsset: 'images/fairplay.jpg',
  ),
  TourismPlace(
    name: 'Lupin',
    genre: 'Crime, Drama, Mystery',
    description: 'Inspired by the adventures of Arsène Lupin, gentleman thief Assane Diop sets out to avenge his father for an injustice inflicted by a wealthy family.',
    score: '78',
    imageAsset: 'images/lupin.jpg',
  ),
  TourismPlace(
    name: 'Pet Sematary: Bloodlines',
    genre: 'Horror, Fantasy',
    description: 'In 1969, a young Jud Crandall has dreams of leaving his hometown of Ludlow, Maine behind, but soon discovers sinister secrets buried within and is forced to confront a dark family history that will forever keep him connected to Ludlow.',
    score: '66',
    imageAsset: 'images/pet.jpg',
  ),
];
